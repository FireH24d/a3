package com.example.demo.Controller;

import com.example.demo.Entity.Product;
import com.example.demo.Service.ProductService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class ProductController {
    private final ProductService productService;

    public ProductController(ProductService productService) {
        this.productService = productService;
    }

    @RequestMapping(value="/product",method= RequestMethod.GET, headers = "Accept=application/json")
    public ResponseEntity<?> getAll() {
        return ResponseEntity.ok(productService.getAll());

    }
    @RequestMapping(value="/product/{id}", method=RequestMethod.GET, headers = "Accept=application/json")
    public ResponseEntity<?> get(@PathVariable long id){
        return ResponseEntity.ok(productService.getById(id));}
    @RequestMapping(value = "/product/{id}", method = RequestMethod.DELETE, headers = "Accept=application/json")
    public void delete(@PathVariable long id){
        productService.delete(id);
    }
    @RequestMapping(value="/product",method=RequestMethod.POST, headers = "Accept=application/json")
    public ResponseEntity<?> update(@RequestBody Product product){
        return ResponseEntity.ok(productService.update(product));
    }
    @RequestMapping(value="/product/cate/{id}",method=RequestMethod.GET, headers = "Accept=application/json")
    public ResponseEntity<?> getcate(@PathVariable long id){
        return ResponseEntity.ok(productService.findAllByCategory_id(id));
    }

}
