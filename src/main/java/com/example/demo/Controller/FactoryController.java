package com.example.demo.Controller;

import com.example.demo.Entity.Factory;
import com.example.demo.Service.FactoryService;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class FactoryController {
    private final FactoryService factoryService;

    public FactoryController(FactoryService factoryService) {
        this.factoryService = factoryService;
    }
    @RequestMapping(value="/factory",method= RequestMethod.GET, headers = "Accept=application/json")
    public ResponseEntity<?> getAll() {
        return ResponseEntity.ok(factoryService.getAll());
    }
    @RequestMapping(value="/factory/{id}", method=RequestMethod.GET, headers = "Accept=application/json")
    public ResponseEntity<?> get(@PathVariable long id){
        return ResponseEntity.ok(factoryService.getById(id));}
    @RequestMapping(value = "/factory/{id}", method = RequestMethod.DELETE, headers = "Accept=application/json")
    public void delete(@PathVariable int id){
        factoryService.delete(id);
    }
    @RequestMapping(value="/factory",method=RequestMethod.POST, headers = "Accept=application/json")
    public ResponseEntity<?> update(@RequestBody Factory factory){
        return ResponseEntity.ok(factoryService.update(factory));
    }
}
