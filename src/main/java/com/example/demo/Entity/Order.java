package com.example.demo.Entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;
import java.util.Optional;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "orderr")
public class Order {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @Column
    private long shop_id;
    private float total_price;
    private Date date;
    private long status;
    @Transient
    private Optional<OrderItem> orderItems;

    public Optional<OrderItem> getOrderItems() {
        return orderItems;
    }

    public void setOrderItems(Optional<OrderItem> orderItems) {
        this.orderItems = orderItems;
    }
}

