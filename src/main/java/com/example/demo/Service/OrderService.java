package com.example.demo.Service;

import com.example.demo.Entity.Order;
import com.example.demo.Repository.OrderRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.util.List;

@Service
public class OrderService {
    private final OrderRepository orderRepository;

    public OrderService(OrderRepository orderRepository) {
        this.orderRepository = orderRepository;
    }

    public List<Order> getAll(){
        return (List<Order>) orderRepository.findAll();
    }
    public ResponseEntity<?> getById(long id) {
        return ResponseEntity.ok(orderRepository.findById(id));
    }

    public void delete(long id){
        orderRepository.deleteById(id);
    }
    public Order update(@RequestBody Order order){
        return  orderRepository.save(order);
    }
    public void save(long shop_id){
        LocalDate date = LocalDate.now();
        float total_price=0;
        orderRepository.insertorder(shop_id,total_price,date,1);
    }
    public ResponseEntity<?> getByShop_id(long id) {
        return ResponseEntity.ok(orderRepository.findByShopId(id));
    }
    public List<Order> getByStatus(long status) {
        return orderRepository.getByStatus(status);
    }


    public void changeStatus(long id, long status) {
        orderRepository.updateStatus(status,id);
    }
}
