package com.example.demo.Service;

import com.example.demo.Entity.Factory;
import com.example.demo.Repository.FactoryRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;
import java.util.Optional;

@Service
public class FactoryService {
    private  final FactoryRepository factoryRepository;

    public FactoryService(FactoryRepository factoryRepository) {
        this.factoryRepository = factoryRepository;
    }

    public ResponseEntity<?> getAll(){
        return ResponseEntity.ok(factoryRepository.findAll());
    }
    public ResponseEntity<?> getById(long id) {
        return ResponseEntity.ok(factoryRepository.findById(id));
    }

    public void delete(long id){
        factoryRepository.deleteById(id);
    }
    public Factory update(@RequestBody Factory factory){
        return  factoryRepository.save(factory);
    }

}
