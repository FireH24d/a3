package com.example.demo.Service;

import com.example.demo.Entity.Order;
import com.example.demo.Entity.OrderItem;
import com.example.demo.Entity.Product;
import com.example.demo.Repository.OrderItemRepository;
import com.example.demo.Repository.OrderRepository;
import com.example.demo.Repository.ProductRepository;
import com.example.demo.Repository.ShopRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class OrderItemService {
    private final OrderItemRepository orderItemRepository;
    private final OrderRepository orderRepository;


    public OrderItemService(OrderItemRepository orderItemRepository, OrderRepository orderRepository) {
        this.orderItemRepository = orderItemRepository;
        this.orderRepository = orderRepository;

    }

    public List<OrderItem> getAll() {
        return (List<OrderItem>) orderItemRepository.findAll();
    }

    public ResponseEntity<?> getById(long id) {
        return ResponseEntity.ok(orderItemRepository.findById(id));
    }

    public void delete(long id) {
        orderItemRepository.deleteById(id);
    }

    public OrderItem update(@RequestBody OrderItem orderItem) {
        return orderItemRepository.save(orderItem);
    }

    public void save(long shop_id, ArrayList<Integer> product_id, ArrayList<Integer> quantity, ArrayList<Float> price) {
        float total_price=0;
        for (int id : product_id) {
            int q = quantity.get(id);
            float p = price.get(id);
            total_price+=q*p;
            Order order = orderRepository.findByShopId(shop_id);
            orderItemRepository.insertorderitem(order.getId(), id, q, p);
            orderRepository.updateprice(total_price,order.getId());

        }

    }
@Transactional
   public List<Order> getByShopID(long shop_id) {
        List<Order> list = orderRepository.findordersbyshop_id(shop_id);
    for(Order order:list){
           order.setOrderItems(orderItemRepository.getByorder_id(order.getId()));
       }
    return list;
    }
   /* @Transactional
    public List<OrderItem[]> getDashboard() {
        List<Order> list1 = (List<Order>) orderRepository.getByStatus(1);
        List<Order> list2 = (List<Order>) orderRepository.getByStatus(2);
        list1.addAll(list2);
        List<OrderItem> itemList = null;

        for(Order order:list2){
            itemList.addAll(orderItemRepository.getByorder_id(order.getId()));
        }







    }*/
}
