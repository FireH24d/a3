package com.example.demo.Repository;

import com.example.demo.Entity.Category;
import com.example.demo.Entity.Handlings;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CategoryRepository extends CrudRepository<Category, Long> {
@Query(value = "SELECT name from category",nativeQuery = true)
List<String> findName();
}
